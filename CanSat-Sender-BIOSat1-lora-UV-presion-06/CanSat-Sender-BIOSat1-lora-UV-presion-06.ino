//lora
#include <SPI.h>
#include <LoRa.h>
//fin lora

#include <DHT.h>
#include <DHT_U.h>

#include <SoftwareSerial.h>

#include <TinyGPS.h>

// presión BMP180
#include <Sodaq_BMP085.h>
#include <Wire.h>

Sodaq_BMP085 bmp; // barometric pression

// fin presión BMP180

/* This sample code demonstrates the normal use of a TinyGPS object.
   It requires the use of SoftwareSerial, and assumes that you have a
   4800-baud serial GPS device hooked up on pins 4(rx) and 3(tx).
*/
// gps
TinyGPS gps;
SoftwareSerial ss(4, 3);
// temp y humedad
#define DHTPIN 5 //Seleccionamos el pin en el que se //conectará el sensor
#define DHTTYPE DHT11 //Se selecciona el DHT11 (hay //otros DHT)
DHT dht(DHTPIN, DHTTYPE); //Se inicia una variable que será usada por Arduino para comunicarse con el sensor

#define magnetico 0

static void smartdelay(unsigned long ms);
static void print_float(float val, float invalid, int len, int prec);
static void print_int(unsigned long val, unsigned long invalid, int len);
static String print_date(TinyGPS &gps);
static void print_str(const char *str, int len);

String cadena = "";
String gpsdata= "";
int val;

//lora
int counter = 0;
int envia = 0;
//fin lora

// UV sensor LM 8511
int UVOUT = A1; //Output from the sensor
int REF_3V3 = A2; //3.3V power on the Arduino board

// fin UV

// PRESIÓN bmp180
//SFE_BMP180 pressure;
// fin PRESIÓN bmp180


void setup()
{
  // BMP180
  bmp.begin(); 
  // fin bmp
  
  // UV
  pinMode(UVOUT, INPUT);
  pinMode(REF_3V3, INPUT);
  
  Serial.begin(9600);
  
  Serial.print("Testing TinyGPS library v. "); Serial.println(TinyGPS::library_version());
  Serial.println("by Mikal Hart");
  Serial.println("Modified by Ricardo Santa -  BIOSAT1 - 2018-2019");
  Serial.println();
  //Serial.println("Sats HDOP Latitude  Longitude  Fix  Date       Time     Date Alt    Course Speed Card  Distance Course Card  Chars Sentences Checksum        DHT11               Campo       Radiación    Presion");
  //Serial.println("          (deg)     (deg)      Age                      Age  (m)    --- from GPS ----  ---- to London  ----  RX    RX        Fail          Humedad    Temp      Magnético      Solar       Atmo.");
  //Serial.println("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
  //Serial.println("Latitude  Longitude  Date       Time  Fix       Alt    Course Speed Card    DHT11               Campo       Radiación    Presion");
  //Serial.println("(deg)     (deg)                       Age       (m)    --- from GPS ----   Humedad    Temp      Magnético      Solar      Atmo.");
  //Serial.println("--------------------------------------------------------------------------------------------------------------------------------");
  Serial.println("Latitude  Longitude  Date       Time  Fix         Alt    DHT11                Campo        Radiación   Presion");
  Serial.println("(deg)     (deg)                       Age         (m)    Humedad    Temp      Magnético      Solar      Atmo.");
  Serial.println("--------------------------------------------------------------------------------------------------------------------------------");

  //ss.begin(4800);
  ss.begin(9600);
  dht.begin(); //Se inicia el sensor
  pinMode(magnetico, INPUT);

  // LORA
  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
    // configuración especial
    LoRa.setTxPower(20);                  // 20 Change the TX power of the radio  values are 2 to 20
    //LoRa.setSpreadingFactor(12);          // 12 ranges from 6-12, default 7 see API docs
    //LoRa.setSignalBandwidth(125E3);       //   Change the signal bandwidth of the radio. Supported values are 7.8E3, 10.4E3, 15.6E3, 20.8E3, 31.25E3, 41.7E3, 62.5E3, 125E3, and 250E3
    //LoRa.setCodingRate4(5);               // 5 Change the coding rate of the radio. Supported values are between 5 and 8, these correspond to coding rates of 4/5 and 4/8. The coding rate numerator is fixed at 4
    
    /*
     * EU433
    Data Rate  Configuration bits/s  Max payload
    DR0         SF12/125kHz    250     59
    DR1         SF11/125kHz    440     59
    DR2         SF10/125kHz    980     59
    DR3         SF9/125kHz    1760    123
    DR4         SF8/125kHz    3125    230
    DR5         SF7/125kHz    5470    230
    DR6         SF7/250kHz    11000   230
    DR7         FSK: 50kpbs   50000   230
    */

  // fin lora
  
}

void loop()
{
  float flat, flon;
  unsigned long age, date, time, chars = 0;
  unsigned short sentences = 0, failed = 0;
  static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;
  cadena = "";
  //print_int(gps.satellites(), TinyGPS::GPS_INVALID_SATELLITES, 5);
  //print_int(gps.hdop(), TinyGPS::GPS_INVALID_HDOP, 5);
  gps.f_get_position(&flat, &flon, &age);  
  //print_float(flat, TinyGPS::GPS_INVALID_F_ANGLE, 8, 6);
   print_str(";",1);
   cadena = String(flat,6)+";";
  //print_float(flon, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
   print_str(";",1);
   cadena += String(flon,6)+";";
  //print_int(age, TinyGPS::GPS_INVALID_AGE, 5);
  gpsdata=print_date(gps);
   //print_str(";",1);
   //gpsdata = return_date(gps);
   cadena += gpsdata+";";
  //print_float(gps.f_altitude(), TinyGPS::GPS_INVALID_F_ALTITUDE, 7, 2);
   //print_str(";",1);
   cadena += String(gps.f_altitude())+";";
   
  //print_float(gps.f_course(), TinyGPS::GPS_INVALID_F_ANGLE, 7, 2);
  //print_float(gps.f_speed_kmph(), TinyGPS::GPS_INVALID_F_SPEED, 6, 2);
  //print_str(gps.f_course() == TinyGPS::GPS_INVALID_F_ANGLE ? "*** " : TinyGPS::cardinal(gps.f_course()), 6);
  
  //print_int(flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0xFFFFFFFF : (unsigned long)TinyGPS::distance_between(flat, flon, LONDON_LAT, LONDON_LON) / 1000, 0xFFFFFFFF, 9);
  //print_float(flat == TinyGPS::GPS_INVALID_F_ANGLE ? TinyGPS::GPS_INVALID_F_ANGLE : TinyGPS::course_to(flat, flon, LONDON_LAT, LONDON_LON), TinyGPS::GPS_INVALID_F_ANGLE, 7, 2);
  //print_str(flat == TinyGPS::GPS_INVALID_F_ANGLE ? "*** " : TinyGPS::cardinal(TinyGPS::course_to(flat, flon, LONDON_LAT, LONDON_LON)), 6);

  //gps.stats(&chars, &sentences, &failed);
  //print_int(chars, 0xFFFFFFFF, 6);
  //print_int(sentences, 0xFFFFFFFF, 10);
  //print_int(failed, 0xFFFFFFFF, 9);
  //// dht1
//  print_str("  ",5);
  //print_float(dht.readHumidity(), 0, 2, 2);
   //print_str(";",1);
   cadena += String(dht.readHumidity())+";";
  //print_str("  ",6);
  //print_float(dht.readTemperature(), 0, 2, 2);
   //print_str(";",1);
   cadena += String(dht.readTemperature())+";";
  // campo magnético
  val = analogRead(magnetico);     // read the input pin
  //print_str("  ",6);
   //print_float(val, 0, 4, 2);
   //print_str(";",1);
   cadena += String(val)+";";
  // radiación solar
  //print_str("  ",6);
  //print_float(0, 0, 4, 2);
  // Presión
  //print_str("  ",6);  
  //print_float(0, 0, 4, 2);

  // UV
  int uvLevel = averageAnalogRead(UVOUT);
  int refLevel = averageAnalogRead(REF_3V3);
  //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
  float outputVoltage = 3.3 / refLevel * uvLevel;
  float uvIntensity = mapfloat(outputVoltage, 0.75, 3.3, 0.0, 15.0); //Convert the voltage to a UV intensity level
  cadena += String(uvIntensity)+";";
  //cadena += String(outputVoltage)+";";
  // UV

  // Presión BMP180
  float bmpTemp = bmp.readTemperature();
  cadena += String(bmpTemp)+";";
  float bmpPresion = bmp.readPressure(); // bmp.readPressure() / 100.0F
  cadena += String(bmpPresion)+";";
  // Calculate altitude assuming 'standard' barometric
  // pressure of 1013.25 millibar = 101325 Pascal
  // TEMP ;Presión ;altura;
  // 22.30;75447.00;2418.54;
  float bmpAltura = bmp.readAltitude(1013.25);
  cadena += String(bmpAltura)+";";
  // Presión BMP180

     
  //Serial.println();
  // enviar via RF - LoRa  --> cadena
  // send packet
  LoRa.beginPacket();
  LoRa.print("BIOSAT01;");
  LoRa.print(counter);
  // alt:lat:lon:temp:luz:Mag
  LoRa.print(";"+cadena);
  LoRa.endPacket();

  counter++;
  // fin lora
  Serial.println(cadena);  
//  Serial.println(analogRead(UVOUT));  // 224
//  Serial.println(uvLevel);            // 218
//  Serial.println(refLevel);           // 873
//  Serial.println(outputVoltage);      // 0.84
//  Serial.println(uvIntensity);        // -1.18
  
  
  
  
 
  smartdelay(500);
}

static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

static void print_float(float val, float invalid, int len, int prec)
{
  if (val == invalid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i=flen; i<len; ++i)
      Serial.print(' ');
  }
  smartdelay(10);
}

static void print_int(unsigned long val, unsigned long invalid, int len)
{
  char sz[32];
  if (val == invalid)
    strcpy(sz, "*******");
  else
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i=strlen(sz); i<len; ++i)
    sz[i] = ' ';
  if (len > 0) 
    sz[len-1] = ' ';
  Serial.print(sz);
  smartdelay(10);
}

static String print_date(TinyGPS &gps)
{
  String gpsdata = "";
  int year;
  byte month, day, hour, minute, second, hundredths;
  unsigned long age;
  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  if (age == TinyGPS::GPS_INVALID_AGE)
    Serial.print("********** ******** ");
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d %02d:%02d:%02d ",
        month, day, year, hour, minute, second);
    //Serial.print(sz);
    gpsdata=String(sz);
  }
  //print_int(age, TinyGPS::GPS_INVALID_AGE, 5);
  smartdelay(10);
  return gpsdata;
}

static String return_date(TinyGPS &gps)
{
  String fecha="********** ********";
  int year;
  byte month, day, hour, minute, second, hundredths;
  unsigned long age;
  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  if (age == TinyGPS::GPS_INVALID_AGE)
  {
    Serial.print("********** ********");
    fecha = "********** ********";    
  }
  else
  {
    fecha = String(atoi(month)) + "/" + String(atoi(day)) + "/" + String(year) + " " + String(atoi(hour)) + ":" + String(atoi(minute)) + ":" + String(atoi(second));
    //char sz[32];
    //sprintf(fecha, "%02d/%02d/%02d %02d:%02d:%02d",
    //    month, day, year, hour, minute, second);
  }
  smartdelay(10);
  return fecha;
}

static void print_str(const char *str, int len)
{
  int slen = strlen(str);
  for (int i=0; i<len; ++i)
    Serial.print(i<slen ? str[i] : ' ');
  smartdelay(10);
}

//Takes an average of readings on a given pin
//Returns the average
int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 16;
  unsigned int runningValue = 0; 

  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;

  return(runningValue);  
}

//The Arduino Map function but for floats
//From: http://forum.arduino.cc/index.php?topic=3922.0
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
