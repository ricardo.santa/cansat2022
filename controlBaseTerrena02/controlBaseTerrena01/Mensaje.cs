﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace controlBaseTerrena01
{
    class Mensaje
    {
        public String IDSatelite;
        public String IDMensaje;
        public String fecha;  // fecha hora del registro
        public String maniobra;    // Prueba, en tierra, en aire, ascenso-descenso, estática
        public Datos datos;     // conjunto de datos de la muestra
        public String rawDatos;   // datos en bruto


        public String toJSON()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }

    class Datos
    {
        public String Lat;
        public String Long;
        public String fechaHora;
        public String Altura;
        public String Humedad;
        public String Temperatura;
        public String Gauss;
        public String uvIntensity;
        public String ExtTemp;
        public String ExtPres;
        public String ExtAltura;
        public String Signal;
    }
}
