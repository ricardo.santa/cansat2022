﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Windows.Forms.DataVisualization.Charting;
using FirebaseNet.Database;

using System.Timers;

using System.Drawing;
using System.Globalization;
using System.Threading;

namespace controlBaseTerrena01
{
    public partial class Form1 : Form
    {
        SerialPort port;

        String datosLeidos = "";
        Series series, seriesTemperatura, seriesHumedad;
        // Data arrays.
        string[] seriesArray = { "Altura", "Temperatura", "Humedad", "Luz", "CampoMagnetico" };
        List<int> pointsArray = new List<int>();
        // satelite
        //  IDSatelite;IDMensaje;Lat;Long;fechaHora;Altura;Humedad;Temperatura;Gauss
        String IDSatelite = "";
        String IDMensaje = "0";
        String fyh = "";
        List<float> Lat = new List<float>();
        List<float> Long = new List<float>();
        List<String> fechaHora = new List<String>();
        List<float> Altura = new List<float>();
        List<float> Humedad = new List<float>();
        List<float> Temperatura = new List<float>();
        List<float> Gauss = new List<float>();
        //
        List<float> uvIntensity = new List<float>(); 
        List<float> ExtTemp = new List<float>();
        List<float> ExtPres = new List<float>(); 
        List<float> ExtAltura = new List<float>();
        //
        List<String> rawDatos = new List<String>();
        //
        Mensaje m0;
        Datos datos;

        // clock
        private static System.Timers.Timer aTimer;

        public Form1()
        {
            InitializeComponent();
            // inicializar el puerto
            //SerialPort port = new SerialPort("COM8", 9600, Parity.None, 8, StopBits.One);

            this.chart1.Palette = ChartColorPalette.SeaGreen;
            this.chart1.Titles.Add("BIOSat1 - Altura");
            this.chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            this.chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            series = this.chart1.Series.Add("Altura");
            series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            // Chart 2
            this.chart2.Palette = ChartColorPalette.SeaGreen;
            this.chart2.Titles.Add("BIOSat1 - Temperatura y Humedad");
            this.chart2.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            this.chart2.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            seriesTemperatura = this.chart2.Series.Add("Temperatura");
            seriesTemperatura.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            seriesHumedad = this.chart2.Series.Add("Humedad");
            seriesHumedad.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            // clock
            aTimer = new System.Timers.Timer();
            aTimer.Interval = 1000;

            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;

            // Have the timer fire repeated events (true is the default)
            aTimer.AutoReset = true;

            // Start the timer
            aTimer.Enabled = true;


        }
        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            this.Invoke(new EventHandler(UpdateClock));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // obtener los puertos disponibles y seleccionar el primero
            toolStripComboBox1_puerto.Items.Clear();
            toolStripComboBox1_puerto.Items.AddRange(SerialPort.GetPortNames());
            try { 
                toolStripComboBox1_puerto.SelectedIndex = 0;
             }
            catch(Exception ex)
            {
                toolStripComboBox1_puerto.SelectedIndex = -1;
            }
            //
            txtManiobra.SelectedIndex = 0;
            //

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (toolStripButton1.Text == "Desconectar")
            {
                port.Close();
                toolStripButton1.Text = "Conectar";
                MessageBox.Show("Puerto cerrado");
            }
            else
            {
                if (port == null && toolStripComboBox1_puerto.SelectedIndex>=0)
                {
                    port = new SerialPort(toolStripComboBox1_puerto.SelectedItem.ToString(), 9600, Parity.None, 8, StopBits.One);
                    port.ReadTimeout = 1000;
                    port.Open();
                    port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                }
                if (port != null)
                {
                    toolStripButton1.Text = "Desconectar";
                    //MessageBox.Show("Puerto abierto y disponible");
                }
            }
            
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Obtenemos el puerto serie que lanza el evento
            SerialPort currentSerialPort = port;
            int intBuffer;
            intBuffer = currentSerialPort.BytesToRead;
            byte[] byteBuffer = new byte[intBuffer];
            //currentSerialPort.Read(byteBuffer, 0, intBuffer);
            //datosLeidos = currentSerialPort.ReadTo("]");
            datosLeidos = currentSerialPort.ReadExisting();
            //this.Invoke(new EventHandler(registrarDato));
            try
            {
                if (toolStripButton1.Text == "Desconectar")
                {
                    this.Invoke(new EventHandler(DoUpDate));
                    this.Invoke(new EventHandler(updateDataScreen));
                }
            }
            catch (Exception ex) { }
            //DoUpDate(sender, new EventArgs());
        }

        private void DoUpDate(object s, EventArgs e)
        {
            //SerialPort currentSerialPort = port;
            //datosLeidos = currentSerialPort.ReadTo("]"); // final del paquete
            String[] dataList;
            //DateTime fyhF = new DateTime();
            String fyhF = "";
            //this.Invoke(new EventHandler(registrarDato));
            this.Invoke(new EventHandler(DisplayText));
            //richTextBox1.Text += datosLeidos;
            // tomar la ultima -1 línea
            int lineaALeer = richTextBox1.Lines.Length - 1;
            if (lineaALeer > 1)

            //if (datosLeidos.StartsWith("[") && datosLeidos.EndsWith("]"))
            {
                // es una mensaje para procesar
                //dataList = datosLeidos.Split(';');
                dataList = richTextBox1.Lines[lineaALeer].Split(';');
                // [BIOSAT01;16021;1000.000000;1000.000000;;1000000.00;16.00;24.00;517;-63]
                //

                // satelite
                //  IDSatelite;IDMensaje;Lat;     Long;      fechaHora;           Altura; Humedad;Temperatura;Gauss;uvIntensity;ExtTemp;ExtPres; ExtAltura; Señal
                //  [BIOSAT01; 14758;    4.829491;-74.549194;10/17/2021 20:33:56 ;1739.30;43.00;  23.00;      510;  0.54;       19.50;  82993.00;1651.57;  -87]

                // validar la cantidad de elementos : 
                // 'BIOSAT01;4025;4.642715;-74.054230;06/01/2019 15:43:51 ;2708.50;13.00;30.00;519;'
                //  IDSatelite;IDMensaje;Lat;Long;fechaHora;Altura;Humedad;Temperatura;Gauss
                if (dataList.Length >= 9)
                {
                    datos = new Datos();
                    try
                    {
                        // tiene una cantidad válida de elementos
                        IDSatelite = dataList[0];

                        IDMensaje = dataList[1];

                        Lat.Add(float.Parse(dataList[2].Replace(".", ",")));
                        datos.Lat = dataList[2];
                        Long.Add(float.Parse(dataList[3].Replace(".", ",")));
                        datos.Long = dataList[3];
                        fyh = dataList[4];
                        if (fyh == "")
                            fyh = "10/17/2019 15:41:28 ";
                        fyhF = DateTime.ParseExact(fyh, "MM/dd/yyyy HH:mm:ss ", CultureInfo.InvariantCulture).ToString();
                        //fyhF = lblFechaHora.Text;                        
                        fechaHora.Add(fyhF);
                        datos.fechaHora = dataList[4];
                        Altura.Add(float.Parse(dataList[5].Replace(".", ",")));
                        datos.Altura = dataList[5];
                        Humedad.Add(float.Parse(dataList[6].Replace(".", ",")));
                        datos.Humedad = dataList[6];
                        Temperatura.Add(float.Parse(dataList[7].Replace(".", ",")));
                        datos.Temperatura = dataList[7];
                        Gauss.Add(float.Parse(dataList[8].Replace(".", ",")));
                        datos.Gauss = dataList[8];

                        //  IDSatelite;IDMensaje;Lat;     Long;      fechaHora;           Altura; Humedad;Temperatura;Gauss;uvIntensity;ExtTemp;ExtPres; ExtAltura; Señal
                        //  [BIOSAT01; 14758;    4.829491;-74.549194;10/17/2021 20:33:56 ;1739.30;43.00;  23.00;      510;  0.54;       19.50;  82993.00;1651.57;  -87]

                        uvIntensity.Add(float.Parse(dataList[9].Replace(".", ",")));
                        datos.uvIntensity = dataList[9];

                        ExtTemp.Add(float.Parse(dataList[10].Replace(".", ",")));
                        datos.ExtTemp = dataList[10];

                        ExtPres.Add(float.Parse(dataList[11].Replace(".", ",")));
                        datos.ExtPres = dataList[11];

                        //ExtAltura.Add(float.Parse(dataList[12].Replace(".", ",")));
                        //datos.ExtAltura = dataList[12];
                        //
                        //datos.Signal = dataList[13].Replace("]", "");
                        rawDatos.Add(richTextBox1.Lines[lineaALeer].ToString());
                        // ahora llevarlos a la gráfica
                        addGraph(series, Altura);
                        addGraph(seriesTemperatura, Temperatura);
                        addGraph(seriesHumedad, Humedad);
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        // mostrar datos en pantalla
                        try
                        {
                            //updateDataScreen(float.Parse(datos.Lat.Replace(".", ",")), float.Parse(datos.Long.Replace(".", ",")), float.Parse(datos.Altura.Replace(".", ",")), float.Parse("0,9"), fyhF, float.Parse(datos.Temperatura.Replace(".", ",")), float.Parse(datos.Humedad.Replace(".", ",")), int.Parse(datos.Gauss), int.Parse(datos.Signal));
                            updateDataScreen(s, e);
                        }
                        catch (Exception ex)
                        {

                        }
                        //datosLeidos = linea;
                        //registrarDato(s, e);
                        //// y ahora a la nube 
                        //registrarDatos(IDSatelite, IDMensaje, datos.fechaHora, txtManiobra.SelectedItem.ToString(), datosLeidos, datos);
                        registrarDatos(IDSatelite, IDMensaje, datos.fechaHora, txtManiobra.SelectedItem.ToString(), richTextBox1.Lines[lineaALeer].ToString(), datos);
                        //MessageBox.Show("datos almacenados");
                        //Thread.Sleep(500);
                    }
                    //}
                }
            }
        }

        private void DisplayText(object sender, EventArgs e)
        {
            richTextBox1.AppendText(datosLeidos);
        }

        private void UpdateClock(object sender, EventArgs e)
        {
            //get current time
            string fecha = DateTime.Now.Date.ToShortDateString();
            int hh = DateTime.Now.Hour;
            int mm = DateTime.Now.Minute;
            int ss = DateTime.Now.Second;

            //time
            string time = "";

            //padding leading zero
            if (hh < 10)
            {
                time += "0" + hh;
            }
            else
            {
                time += hh;
            }
            time += ":";

            if (mm < 10)
            {
                time += "0" + mm;
            }
            else
            {
                time += mm;
            }
            time += ":";

            if (ss < 10)
            {
                time += "0" + ss;
            }
            else
            {
                time += ss;
            }

            //update label
            lblFechaHora.Text = fecha + " " +time;
        }

        private void updateDataScreen(object sender, EventArgs e)
        //private void updateDataScreen(float lat, float longitud, float alt, float vel, DateTime fechahora, float temp, float humedad, int gauss, int senal)
        {
            if (Altura.Count > 1)
            {
                lblAltura.Text = "Altura: " + Altura[Altura.Count - 1].ToString() + "mts";
                lblVelocidad.Text = "Velocidad: " + "10.0".ToString() + "km/h";
                //lblTemperatura.Text = "Temperatura:" + Temperatura[Temperatura.Count - 1].ToString() + "C";
                lblTemperatura.Text = "Temperatura:" + ExtTemp[ExtTemp.Count - 1].ToString() + "C";
                lblHumedad.Text = "Humedad R: " + Humedad[Humedad.Count - 1].ToString() + "%";
                lblLat.Text = "Latitud: " + Lat[Lat.Count - 1].ToString();
                lblLong.Text = "Longitud: " + Long[Long.Count - 1].ToString();
                lblLuz.Text =  "Luz: " + uvIntensity[uvIntensity.Count - 1].ToString();
                lblGauss.Text = "Gauss: " + Gauss[Gauss.Count - 1].ToString();
            }

        }

        private void addGraph(Series series, List<float> dataList)
        {
            if (series.Points.Count > 500)
                series.Points.RemoveAt(0);
            //punto.XValue = pointsArray[pointsArray.Count - 1];
            //punto.YValues.SetValue(pointsArray[pointsArray.Count - 1], 0);
            series.Points.Add(dataList[dataList.Count - 1]);
            //series.Points.Add(punto);
            //series.Points.AddXY(pointsArray.Count + 1, pointsArray[pointsArray.Count - 1]);
            // nube
            //if (series.Points.Count > 1 && series.Points.Count % 10 == 0)
            //    registrarDatos(dataList);
        }

        //                          IDSatelite, IDMensaje, fechaHora.Last(), TxtManiobra.Text, datosLeidos, datos
        private void registrarDatos(String IDSatelite, String IDMensaje, String fechaHora, String maniobra, String rawDatos, Datos datos)
        {

            // toma una serie de datos y los almacena
            // ¿?
            // Instanciating with base URL  
            FirebaseDB firebaseDB = new FirebaseDB("https://projects-5dd43.firebaseio.com");

            // Referring to Node with name "Teams"  
            FirebaseDB firebaseDBTeams = firebaseDB.Node("BIOSAT1");
            
            m0 = new Mensaje();
            m0.IDSatelite = IDSatelite;
            m0.IDMensaje = IDMensaje;
            m0.fecha = fechaHora;           //DateTime.Now;
            m0.maniobra= maniobra;
            m0.rawDatos = rawDatos;
            m0.datos = datos;
            
            FirebaseResponse postResponse = firebaseDBTeams.Post(m0.toJSON());
            //FirebaseResponse postResponse = firebaseDBTeams.Post(data);
            //MessageBox.Show(postResponse.Success.ToString());

        }

        private void procesarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String [] dataList;
            DateTime fyhF = new DateTime();
            // leer una a una las líneas del txt y procesarlas
            foreach (String linea in richTextBox1.Lines)
            {
                dataList = linea.Split(';');

                // [BIOSAT01;16021;1000.000000;1000.000000;;1000000.00;16.00;24.00;517;-63]
                //

                // validar la cantidad de elementos : 
                // 'BIOSAT01;4025;4.642715;-74.054230;06/01/2019 15:43:51 ;2708.50;13.00;30.00;519;'
                //  IDSatelite;IDMensaje;Lat;Long;fechaHora;Altura;Humedad;Temperatura;Gauss
                if (dataList.Length >= 9)
                {
                    datos = new Datos();
                    try
                    {
                        // tiene una cantidad válida de elementos
                        IDSatelite = dataList[0];

                        IDMensaje = dataList[1];

                        Lat.Add(float.Parse(dataList[2].Replace(".", ",")));
                        datos.Lat = dataList[2];
                        Long.Add(float.Parse(dataList[3].Replace(".", ",")));
                        datos.Long = dataList[3];
                        fyh = dataList[4]; //10/17/2019 15:41:28 
                        if (fyh == "")
                            fyh = "10/17/2019 15:41:28 ";
                        fyhF = DateTime.ParseExact(fyh, "MM/dd/yyyy HH:mm:ss ", CultureInfo.InvariantCulture);
                        fechaHora.Add(fyhF.ToString());
                        datos.fechaHora = dataList[4];
                        Altura.Add(float.Parse(dataList[5].Replace(".", ",")));
                        datos.Altura = dataList[5];
                        Humedad.Add(float.Parse(dataList[6].Replace(".", ",")));
                        datos.Humedad = dataList[6];
                        Temperatura.Add(float.Parse(dataList[7].Replace(".", ",")));
                        datos.Temperatura = dataList[7];
                        Gauss.Add(float.Parse(dataList[8].Replace(".", ",")));
                        datos.Gauss = dataList[8];

                        //ojo

                        datos.Signal = dataList[9].Replace("]","");
                        rawDatos.Add(datosLeidos);
                        // ahora llevarlos a la gráfica
                        addGraph(series, Altura);
                        addGraph(seriesTemperatura, Temperatura);
                        addGraph(seriesHumedad, Humedad);
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        // mostrar datos en pantalla
                        try
                        {
                            //updateDataScreen(float.Parse(datos.Lat.Replace(".", ",")), float.Parse(datos.Long.Replace(".", ",")), float.Parse(datos.Altura.Replace(".", ",")), float.Parse("0,9"), fyhF, float.Parse(datos.Temperatura.Replace(".", ",")), float.Parse(datos.Humedad.Replace(".", ",")), int.Parse(datos.Gauss), int.Parse(datos.Signal));
                            updateDataScreen(sender, e);
                        }
                        catch(Exception ex)
                        {

                        }
                        //datosLeidos = linea;
                        //registrarDato(sender, e);
                        //// y ahora a la nube 
                        //registrarDatos(IDSatelite, IDMensaje, datos.fechaHora, txtManiobra.SelectedItem.ToString(), datosLeidos, datos);
                        //MessageBox.Show("datos almacenados");
                        //Thread.Sleep(500);
                    }
                    //}
                }
            }
        }

        private void registrarDato(object s, EventArgs e)
        {

            // toma una serie de datos y los almacena
            // ¿?
            // Instanciating with base URL  
            FirebaseDB firebaseDB = new FirebaseDB("https://projects-5dd43.firebaseio.com");

            // Referring to Node with name "Teams"  
            FirebaseDB firebaseDBTeams = firebaseDB.Node("DatosBIOSAT1");

            m0 = new Mensaje();
            m0.IDSatelite = "Biosat1";
            m0.IDMensaje = "0";
            m0.fecha = DateTime.Now.ToLongTimeString();
            m0.maniobra = this.txtManiobra.SelectedText;
            m0.rawDatos = datosLeidos;
            //m0.datos = null;

            FirebaseResponse postResponse = firebaseDBTeams.Post(m0.toJSON());
            //FirebaseResponse postResponse = firebaseDBTeams.Post(data);
            //MessageBox.Show(postResponse.Success.ToString());
            //DisplayText(s, e);

        }

        private void Form1_FromClosing(object sender, EventArgs e)
        {
            port.Close();
        }

    }
}
