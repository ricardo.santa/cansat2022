#include <SPI.h>
#include <LoRa.h>

String mensaje="";
void setup() {
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Receiver");

  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  // register the receive callback
  LoRa.onReceive(onReceive);

  // put the radio into receive mode
  LoRa.receive();
}

void loop() {
//  // try to parse packet
//  //Serial.print("recibiendo... ");
//  //Serial.println(LoRa.packetRssi());
//  int packetSize = LoRa.parsePacket();
//  if (packetSize) {
//    // received a packet
//    Serial.print("Received packet '");
//
//    // read packet
//    while (LoRa.available()) {
//      Serial.print((char)LoRa.read());
//    }
//
//    // print RSSI of packet
//    Serial.print("' with RSSI ");
//    Serial.println(LoRa.packetRssi());
//  }
}

void onReceive(int packetSize) {
  mensaje = "";
  // received a packet
  //Serial.print("Received packet '");
  
  // read packet
  for (int i = 0; i < packetSize; i++) {
    //Serial.print((char)LoRa.read());
    mensaje += (char)LoRa.read();
  }
  Serial.print("[");
  Serial.print(mensaje);
  Serial.print(LoRa.packetRssi());
  Serial.println("]");  // caracter de fin de mensaje para la lectura en el pc
  delay(100);
  // print RSSI of packet
  //Serial.print("' with RSSI ");
  //Serial.println(LoRa.packetRssi());
}
